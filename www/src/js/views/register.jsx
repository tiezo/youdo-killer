var Register = Register || {};

Register.view = function (ctrl) {
    return (
        <div class="views">
            <div class="view view-main">
                <div class="content-block-title">Регистрация</div>
                <div class="list-block inputs-list">
                    <ul>
                        <li>
                            <div class="item-content">
                                <div class="item-media"><i class="icon icon-form-email"></i></div>
                                <div class="item-inner">
                                    <div class="item-title floating-label">Эл. почта</div>
                                    <div class="item-input">
                                        <input type="email" placeholder=""
                                            onchange={m.withAttr('value', ctrl.email)}/>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="item-content">
                                <div class="item-media"><i class="icon icon-form-email"></i></div>
                                <div class="item-inner">
                                    <div class="item-title floating-label">Пароль</div>
                                    <div class="item-input">
                                        <input type="password" placeholder=""
                                            onchange={m.withAttr('value', ctrl.password)}/>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="content-block">
                    <p class="buttons-row">
                        <a class="button button-fill button-raised"
                           onclick={ctrl.Register.bind(ctrl, ctrl.email, ctrl.password)}>
                            Зарегистрироваться
                        </a>
                    </p>
                </div>
                <div class="content-block">
                    <a onclick={ctrl.RedirectTo('/login')}>Войти</a>
                </div>
            </div>
        </div>
    )
};
