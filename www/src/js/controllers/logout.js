var Logout = Logout || {};

Logout.controller = function () {
	UserService.logout();

	m.route('/login');
};

