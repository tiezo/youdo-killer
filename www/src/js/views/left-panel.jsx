var LeftPanel = LeftPanel || {};

LeftPanel.view = function (ctrl, attrs, children) {
    return (
        <div>
            <div class="statusbar-overlay"></div>
            <div class="panel-overlay"></div>
            <div class="panel panel-left panel-cover">
                <div class="list-block">
                    <ul>
                        <li>
                            <a class="item-link close-panel">
                                <div class="item-content">
                                    <div class="item-inner">
                                        <div class="item-title" onclick={ctrl.RedirectTo('/tasks')}>
                                            Задачи
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a class="item-link close-panel">
                                <div class="item-content">
                                    <div class="item-inner">
                                        <div class="item-title" onclick={ctrl.RedirectTo('/logout')}>
                                            Выход
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    )
};