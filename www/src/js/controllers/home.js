var Home = Home || {};

Home.controller = function () {
    if (!UserService.isLoggedIn()) {
        return m.route('/login');
    }

    m.route('/tasks');
};
