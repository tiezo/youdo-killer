// TODO вынести работу с текущим юзером в currentUser.js?

var UserService = {
    register: function (email, password) {
        return Backendless.request([Backendless.restUrls.users, 'register'].join('/'), 'POST', {
            email: email,
            password: password
        }, function (data) {
            // вызываем login, автологина при регистрации у baas нет
            UserService.login(email, password);
        }, function (data) {
            alert('Возникла ошибка. Возможно, пользователь с такой эл. почтой уже существует.');
        });
    },
    login: function (login, password) {
        return Backendless.request([Backendless.restUrls.users, 'login'].join('/'), 'POST', {
            login: login,
            password: password
        }, function (data) {
            // clear previous data
            Storage.clear();

            Storage.set('currentUser', {
                id: data['objectId'],
                token: data['user-token']
            });

            return m.route('/');
        }, function (data) {
            alert('Возникла ошибка. Проверьте правильность логина и пароля');
        });
    },


    isLoggedIn: function () {
        var current_user = this.getCurrentUser();

        return current_user !== null;
    },
    logout: function () {
        Storage.clear();
    },


    getCurrentUser: function () {
        return Storage.get('currentUser');
    },
    getId: function () {
        if (!this.isLoggedIn()) {
            return false;
        }

        var user = Storage.get('currentUser');
        return user.id;
    },
    getToken: function () {
        if (!this.isLoggedIn()) {
            return false;
        }

        var user = Storage.get('currentUser');
        return user.token;
    }
};
