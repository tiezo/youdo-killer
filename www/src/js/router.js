// роутинг

m.route.mode = 'hash';

m.route(document.body, "/", {
    "": Home,

    "/login": Login,
    "/register": Register,
    "/logout": Logout,

    "/tasks": Tasks
});
