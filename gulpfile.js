var gulp = require('gulp');
var msx = require('gulp-msx');
var concat = require('gulp-concat');

var js_root = './www/src/js';

// обработка скриптов
gulp.task('scripts', function () {
    return gulp.src([
    		js_root + '/models/*.js', 
    		js_root + '/controllers/*.js',
    		js_root + '/views/*.jsx',
    		js_root + '/*.js'
		])
        .pipe(msx())
        .pipe(concat('app.js'))
        .pipe(gulp.dest('./www/dist'))
});

gulp.task('watch', function () {
	gulp.watch(js_root + '/**', ['scripts']);
});

gulp.task('default', ['watch']);

// при запуске команды gulp явно выполняем сборку - таск watch почему-то не стартует сам
gulp.run('scripts')
