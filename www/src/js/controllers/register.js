var Register = Register || {};

Register.controller = function () {
    this.email = m.prop();
    this.password = m.prop();

    this.Register = function (email, password) {
        if (!email() || !password()) {
            // TODO: обработка ошибок
            alert('Возникла ошибка. Возможно, пользователь с такой эл. почтой существует');
            return;
        }

        UserService.register(email(), password());
    };

    this.RedirectTo = function (path) {
        return function () {
            m.route(path);
        };
    }
};
