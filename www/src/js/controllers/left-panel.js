var LeftPanel = LeftPanel || {};

LeftPanel.controller = function () {
    // вызвать m.route(path) и костыльно скрыть оверлей
    this.RedirectTo = function (path) {
        return function () {
            // костыль. удаляем маску меню
            document.querySelector('body').classList.remove('with-panel-' + myApp.params.swipePanel + '-cover');

            m.route(path);
        }
    };
};
