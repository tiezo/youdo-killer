/**
 * взаимодействие с апи
 */

var Backendless = {
    config: {
        // TODO: вынести в config.json
        APP_URL: 'https://api.backendless.com',
        APP_VERSION: 'v1',
        APP_ID: 'A7311643-57A1-37F3-FFFD-94602EDF1800',
        SECRET_KEY: 'FBB76A35-1DE5-8471-FF58-6F8A49585F00'
    },

    request: function (action, method, data, successCallback, errorCallback) {
        return m.request({
            method: method,
            url: [this.config.APP_URL, this.config.APP_VERSION, action].join('/'),
            data: data,
            config: (function (xhr) {
                xhr.setRequestHeader('application-id', this.config.APP_ID);
                xhr.setRequestHeader('secret-key', this.config.SECRET_KEY);
            }).bind(this)
        }).then(successCallback, errorCallback);
    },

    restUrls: {
        users: 'users'
    }
};

/**
 * обертка над localStorage
 */

var Storage = {
    set: function (key, value) {
        localStorage.setItem(key, JSON.stringify(value));
    },
    get: function (key) {
        return JSON.parse(localStorage.getItem(key));
    },
    clear: function () {
        localStorage.clear();
    }
};

// TODO вынести работу с текущим юзером в currentUser.js?

var UserService = {
    register: function (email, password) {
        return Backendless.request([Backendless.restUrls.users, 'register'].join('/'), 'POST', {
            email: email,
            password: password
        }, function (data) {
            // вызываем login, автологина при регистрации у baas нет
            UserService.login(email, password);
        }, function (data) {
            alert('Возникла ошибка. Возможно, пользователь с такой эл. почтой уже существует.');
        });
    },
    login: function (login, password) {
        return Backendless.request([Backendless.restUrls.users, 'login'].join('/'), 'POST', {
            login: login,
            password: password
        }, function (data) {
            // clear previous data
            Storage.clear();

            Storage.set('currentUser', {
                id: data['objectId'],
                token: data['user-token']
            });

            return m.route('/');
        }, function (data) {
            alert('Возникла ошибка. Проверьте правильность логина и пароля');
        });
    },


    isLoggedIn: function () {
        var current_user = this.getCurrentUser();

        return current_user !== null;
    },
    logout: function () {
        Storage.clear();
    },


    getCurrentUser: function () {
        return Storage.get('currentUser');
    },
    getId: function () {
        if (!this.isLoggedIn()) {
            return false;
        }

        var user = Storage.get('currentUser');
        return user.id;
    },
    getToken: function () {
        if (!this.isLoggedIn()) {
            return false;
        }

        var user = Storage.get('currentUser');
        return user.token;
    }
};


var Home = Home || {};

Home.controller = function () {
    if (!UserService.isLoggedIn()) {
        return m.route('/login');
    }

    m.route('/tasks');
};

var LeftPanel = LeftPanel || {};

LeftPanel.controller = function () {
    // вызвать m.route(path) и костыльно скрыть оверлей
    this.RedirectTo = function (path) {
        return function () {
            // костыль. удаляем маску меню
            document.querySelector('body').classList.remove('with-panel-' + myApp.params.swipePanel + '-cover');

            m.route(path);
        }
    };
};

var Login = Login || {};

Login.controller = function () {
    this.login = m.prop();
    this.password = m.prop();

    this.Login = function (login, password) {
        if (!login() || !password()) {
            alert('заменить вывод ошибки на всплывающее снизу окно');
            return;
        }

        UserService.login(login(), password());
    };

    this.RedirectTo = function (path) {
        return function () {
            // TODO: animations?
            m.route(path);
        }
    };
};

var Logout = Logout || {};

Logout.controller = function () {
	UserService.logout();

	m.route('/login');
};


var Register = Register || {};

Register.controller = function () {
    this.email = m.prop();
    this.password = m.prop();

    this.Register = function (email, password) {
        if (!email() || !password()) {
            // TODO: обработка ошибок
            alert('Возникла ошибка. Возможно, пользователь с такой эл. почтой существует');
            return;
        }

        UserService.register(email(), password());
    };

    this.RedirectTo = function (path) {
        return function () {
            m.route(path);
        };
    }
};

var Home = Home || {};

Home.view = function (ctrl, attrs) {
	return "foo";
};
var LeftPanel = LeftPanel || {};

LeftPanel.view = function (ctrl, attrs, children) {
    return (
        {tag: "div", attrs: {}, children: [
            {tag: "div", attrs: {class:"statusbar-overlay"}}, 
            {tag: "div", attrs: {class:"panel-overlay"}}, 
            {tag: "div", attrs: {class:"panel panel-left panel-cover"}, children: [
                {tag: "div", attrs: {class:"list-block"}, children: [
                    {tag: "ul", attrs: {}, children: [
                        {tag: "li", attrs: {}, children: [
                            {tag: "a", attrs: {class:"item-link close-panel"}, children: [
                                {tag: "div", attrs: {class:"item-content"}, children: [
                                    {tag: "div", attrs: {class:"item-inner"}, children: [
                                        {tag: "div", attrs: {class:"item-title", onclick:ctrl.RedirectTo('/tasks')}, children: [
                                            "Задачи"
                                        ]}
                                    ]}
                                ]}
                            ]}
                        ]}, 
                        {tag: "li", attrs: {}, children: [
                            {tag: "a", attrs: {class:"item-link close-panel"}, children: [
                                {tag: "div", attrs: {class:"item-content"}, children: [
                                    {tag: "div", attrs: {class:"item-inner"}, children: [
                                        {tag: "div", attrs: {class:"item-title", onclick:ctrl.RedirectTo('/logout')}, children: [
                                            "Выход"
                                        ]}
                                    ]}
                                ]}
                            ]}
                        ]}
                    ]}
                ]}
            ]}
        ]}
    )
};
var Login = Login || {};

Login.view = function (ctrl) {
    return (
        {tag: "div", attrs: {class:"views"}, children: [
            {tag: "div", attrs: {class:"view view-main"}, children: [
                {tag: "div", attrs: {class:"content-block-title"}, children: ["Войти"]}, 
                {tag: "div", attrs: {class:"list-block inputs-list"}, children: [
                    {tag: "ul", attrs: {}, children: [
                        {tag: "li", attrs: {}, children: [
                            {tag: "div", attrs: {class:"item-content"}, children: [
                                {tag: "div", attrs: {class:"item-media"}, children: [{tag: "i", attrs: {class:"icon icon-form-email"}}]}, 
                                {tag: "div", attrs: {class:"item-inner"}, children: [
                                    {tag: "div", attrs: {class:"item-title floating-label"}, children: ["Эл. почта"]}, 
                                    {tag: "div", attrs: {class:"item-input"}, children: [
                                        {tag: "input", attrs: {type:"email", placeholder:"", 
                                               onchange:m.withAttr('value', ctrl.login)}}
                                    ]}
                                ]}
                            ]}
                        ]}, 
                        {tag: "li", attrs: {}, children: [
                            {tag: "div", attrs: {class:"item-content"}, children: [
                                {tag: "div", attrs: {class:"item-media"}, children: [{tag: "i", attrs: {class:"icon icon-form-email"}}]}, 
                                {tag: "div", attrs: {class:"item-inner"}, children: [
                                    {tag: "div", attrs: {class:"item-title floating-label"}, children: ["Пароль"]}, 
                                    {tag: "div", attrs: {class:"item-input"}, children: [
                                        {tag: "input", attrs: {type:"password", placeholder:"", 
                                               onchange:m.withAttr('value', ctrl.password)}}
                                    ]}
                                ]}
                            ]}
                        ]}
                    ]}
                ]}, 
                {tag: "div", attrs: {class:"content-block"}, children: [
                    {tag: "p", attrs: {class:"buttons-row"}, children: [
                        {tag: "a", attrs: {class:"button button-fill button-raised", 
                           onclick:ctrl.Login.bind(ctrl, ctrl.login, ctrl.password)}, children: [
                            "Войти"
                        ]}
                    ]}
                ]}, 
                {tag: "div", attrs: {class:"content-block"}, children: [
                    {tag: "a", attrs: {onclick:ctrl.RedirectTo('/register')}, children: ["Регистрация"]}
                ]}
            ]}
        ]}
    )
};
var NavBar = NavBar || {};

NavBar.view = function () {
    return (
        {tag: "div", attrs: {class:"navbar"}, children: [
            {tag: "div", attrs: {class:"navbar-inner"}, children: [
                {tag: "div", attrs: {class:"left"}, children: [
                    {tag: "a", attrs: {href:"#", class:"link icon-only open-panel"}, children: [{tag: "i", attrs: {class:"icon icon-bars"}}]}
                ]}, 
                {tag: "div", attrs: {class:"centered"}, children: [attrs.title]}
            ]}
        ]}
    );
};
var Register = Register || {};

Register.view = function (ctrl) {
    return (
        {tag: "div", attrs: {class:"views"}, children: [
            {tag: "div", attrs: {class:"view view-main"}, children: [
                {tag: "div", attrs: {class:"content-block-title"}, children: ["Регистрация"]}, 
                {tag: "div", attrs: {class:"list-block inputs-list"}, children: [
                    {tag: "ul", attrs: {}, children: [
                        {tag: "li", attrs: {}, children: [
                            {tag: "div", attrs: {class:"item-content"}, children: [
                                {tag: "div", attrs: {class:"item-media"}, children: [{tag: "i", attrs: {class:"icon icon-form-email"}}]}, 
                                {tag: "div", attrs: {class:"item-inner"}, children: [
                                    {tag: "div", attrs: {class:"item-title floating-label"}, children: ["Эл. почта"]}, 
                                    {tag: "div", attrs: {class:"item-input"}, children: [
                                        {tag: "input", attrs: {type:"email", placeholder:"", 
                                            onchange:m.withAttr('value', ctrl.email)}}
                                    ]}
                                ]}
                            ]}
                        ]}, 
                        {tag: "li", attrs: {}, children: [
                            {tag: "div", attrs: {class:"item-content"}, children: [
                                {tag: "div", attrs: {class:"item-media"}, children: [{tag: "i", attrs: {class:"icon icon-form-email"}}]}, 
                                {tag: "div", attrs: {class:"item-inner"}, children: [
                                    {tag: "div", attrs: {class:"item-title floating-label"}, children: ["Пароль"]}, 
                                    {tag: "div", attrs: {class:"item-input"}, children: [
                                        {tag: "input", attrs: {type:"password", placeholder:"", 
                                            onchange:m.withAttr('value', ctrl.password)}}
                                    ]}
                                ]}
                            ]}
                        ]}
                    ]}
                ]}, 
                {tag: "div", attrs: {class:"content-block"}, children: [
                    {tag: "p", attrs: {class:"buttons-row"}, children: [
                        {tag: "a", attrs: {class:"button button-fill button-raised", 
                           onclick:ctrl.Register.bind(ctrl, ctrl.email, ctrl.password)}, children: [
                            "Зарегистрироваться"
                        ]}
                    ]}
                ]}, 
                {tag: "div", attrs: {class:"content-block"}, children: [
                    {tag: "a", attrs: {onclick:ctrl.RedirectTo('/login')}, children: ["Войти"]}
                ]}
            ]}
        ]}
    )
};

var Tasks = Tasks || {};

Tasks.view = function (ctrl) {
    return (
        {tag: "div", attrs: {class:"jsx-component-wrapper"}, children: [
            LeftPanel, 
            {tag: "div", attrs: {class:"views"}, children: [
                {tag: "div", attrs: {class:"view view-main"}, children: [
                    m.component(NavBar, {title:"Задачи"}), 
                    {tag: "div", attrs: {class:"pages navbar-through toolbar-through"}, children: [
                        {tag: "div", attrs: {"data-page":"index", class:"page"}, children: [
                            {tag: "div", attrs: {class:"page-content"}, children: [
                                {tag: "div", attrs: {class:"content-block"}, children: [
                                    {tag: "p", attrs: {}}, 
                                    {tag: "a", attrs: {href:"about.html"}, children: ["About app"]}
                                ]}
                            ]}
                        ]}
                    ]}
                ]}
            ]}
        ]}
    )
};

// роутинг

m.route.mode = 'hash';

m.route(document.body, "/", {
    "": Home,

    "/login": Login,
    "/register": Register,
    "/logout": Logout,

    "/tasks": Tasks
});
