var Tasks = Tasks || {};

Tasks.view = function (ctrl) {
    return (
        <div class="jsx-component-wrapper">
            <LeftPanel/>
            <div class="views">
                <div class="view view-main">
                    <NavBar title="Задачи"/>
                    <div class="pages navbar-through toolbar-through">
                        <div data-page="index" class="page">
                            <div class="page-content">
                                <div class="content-block">
                                    <p></p>
                                    <a href="about.html">About app</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
};
