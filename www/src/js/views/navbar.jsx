var NavBar = NavBar || {};

NavBar.view = function () {
    return (
        <div class="navbar">
            <div class="navbar-inner">
                <div class="left">
                    <a href="#" class="link icon-only open-panel"><i class="icon icon-bars"></i></a>
                </div>
                <div class="centered">{attrs.title}</div>
            </div>
        </div>
    );
};