/**
 * взаимодействие с апи
 */

var Backendless = {
    config: {
        // TODO: вынести в config.json
        APP_URL: 'https://api.backendless.com',
        APP_VERSION: 'v1',
        APP_ID: 'A7311643-57A1-37F3-FFFD-94602EDF1800',
        SECRET_KEY: 'FBB76A35-1DE5-8471-FF58-6F8A49585F00'
    },

    request: function (action, method, data, successCallback, errorCallback) {
        return m.request({
            method: method,
            url: [this.config.APP_URL, this.config.APP_VERSION, action].join('/'),
            data: data,
            config: (function (xhr) {
                xhr.setRequestHeader('application-id', this.config.APP_ID);
                xhr.setRequestHeader('secret-key', this.config.SECRET_KEY);
            }).bind(this)
        }).then(successCallback, errorCallback);
    },

    restUrls: {
        users: 'users'
    }
};
