var Login = Login || {};

Login.controller = function () {
    this.login = m.prop();
    this.password = m.prop();

    this.Login = function (login, password) {
        if (!login() || !password()) {
            alert('заменить вывод ошибки на всплывающее снизу окно');
            return;
        }

        UserService.login(login(), password());
    };

    this.RedirectTo = function (path) {
        return function () {
            // TODO: animations?
            m.route(path);
        }
    };
};
